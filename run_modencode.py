from gbdb.gbdbup_class import DBParser

import numpy as np
import logging
logging.basicConfig(level=logging.DEBUG)

initial = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/all_modencode.csv'
directory = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/MODENCODE_files_test'
further = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/ex_out_modencode.txt'
final   = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/processed_files.csv'
#reload(gb)
a = DBParser()
# modENCODE
a.dbLoadInfo(database='modENCODE', mode='FILE', filename=initial, format='csv', sep='\t', header=0, index_col=False, na_filter=False)
#a.dbParse(further, 4)
a.dbDownload(directory, 4, meta_file=further, db_type='MODENCODE')
##a._is_downloaded = True
#a.dbMerge(directory, 4, meta_file=further, db_type='MODENCODE', write_output_to=final)
#a.dbProcessFiles(1, directory= directory, partial=[0,5], db_type='MODENCODE', write_output_to=final)
