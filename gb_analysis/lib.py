from __future__ import division, print_function
import pandas as pd
import numpy as np
#import seaborn as sns
import os
import logging
import itertools
from multiprocessing import Pool, Lock

logging.basicConfig(level=logging.DEBUG)

ANN_DICT = {'Intergenic': 'Intergenic',
            'intron': 'Intron',
            'TSS': 'TSS',
            'TTS': 'TTS',
            "5' UTR ": '5UTR',
            "3' UTR ": '3UTR',
            'promoter-TSS': 'promoterTSS',
            'exon ': 'Exon'}

def get_short_description(x):
    #print(x)
    for k in ANN_DICT.keys():
        try:
            if k in x:
                return ANN_DICT[k]
        except Exception:
            pass
    return ''

def jaccard_index(set_1, set_2):
    set_1 = set(set_1)
    set_2 = set(set_2)
    return len(set_1.intersection(set_2)) / float(len(set_1.union(set_2)))

def get_descriptive_stats(homer_file):

    logging.debug("Producing descriptive stats for: {}".format(homer_file))
    df      = pd.read_csv(homer_file, sep='\t', header=0, index_col=False)
    df.loc[:,'Annotation_simple'] = df.loc[:, 'Annotation'].apply(get_short_description)
    mask    = np.logical_or(np.logical_and(df['Annotation_simple'] != 'Intergenic', df['Annotation_simple'] != ''), abs(df['Distance to TSS']) < 2000)
    df_f = df[mask]

    res = {}

    res['#peaks_total']     = len(df)
    res['#genes_nearest']   = len(df_f)

    df_g = df.groupby('Annotation_simple')
    for v in ANN_DICT.values():
        if v in df_g.groups:
            res['#' + v]      = df_g.get_group(v).count()['Chr']
        else:
            res['#' + v]    = 0

    return res

def produce_description(meta_file, directory=None, output_file=None):

    meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)
    if directory:
        meta['Local filename new'] = meta['Local filename'].apply(lambda x: os.path.abspath(directory) + '/' + x.split('/')[-1])
    else:
        meta['Local filename new'] = meta['Local filename']

    for i, row in meta.iterrows():
        res = get_descriptive_stats(row['Local filename new'])
        for k in res.keys():
            meta.loc[i, k] = res[k]

    if output_file:
        meta.to_csv(output_file, sep='\t', index=False)

    return meta

def compare_two_sets(hf1, hf2, measurement_function):
    #logging.debug("Pairwise comparison for files: \n\t{}\n\t{}".format(hf1, hf2))

    df      = pd.read_csv(hf1, sep='\t', header=0, index_col=False)
    m = df.as_matrix()
    ann_s = np.array( [get_short_description(x) for x in m[:, df.columns.get_loc("Annotation")] ] )
    df.loc[:,'Annotation_simple'] = ann_s
    #mask    = np.logical_or(np.logical_and(df['Annotation_simple'] != 'Intergenic', df['Annotation_simple'] != ''), abs(df['Distance to TSS']) < 2000)
    mask    = np.logical_or(np.logical_and(ann_s != 'Intergenic', ann_s != ''), abs(m[:,df.columns.get_loc('Distance to TSS')]) < 2000)
    print(len(mask))
    df_f1 = df[mask].copy(deep=True).as_matrix()

    df = pd.read_csv(hf2, sep='\t', header=0, index_col=False)
    m = df.as_matrix()
    #ann_s = np.apply_along_axis(get_short_description, 0, m[:, df.columns.get_loc("Annotation")])
    ann_s = np.array( [get_short_description(x) for x in m[:, df.columns.get_loc("Annotation")] ] )
    df.loc[:,'Annotation_simple'] = ann_s
    #df.loc[:,'Annotation_simple'] = df.loc[:, 'Annotation'].apply(get_short_description)
    mask    = np.logical_or(np.logical_and(ann_s != 'Intergenic', ann_s != ''), abs(m[:,df.columns.get_loc('Distance to TSS')]) < 2000)
    df_f2 = df[mask].copy(deep=True).as_matrix()

    ind = df.columns.get_loc("Entrez ID")

    res = measurement_function(df_f1[:,ind], df_f2[:,ind])
    return res

def compare_two_sets_prop(hf1, hf2, funct):
    #logging.debug("Pairwise comparison for files: \n\t{}\n\t{}".format(hf1, hf2))
    #print(hf1, hf2)
    df      = pd.read_csv(hf1, sep='\t', header=0, index_col=False)
    m = df.as_matrix()
    ar = m[:,df.columns.get_loc('Distance to TSS')]
    ann_s = copy(m[:,df.columns.get_loc('Distance to TSS')])
    for k, v in ANN_DICT.iteritems(): ann_s[ar == k] = v
    mask    = np.logical_or(np.logical_and(ann_s != 'Intergenic', ann_s != ''), abs(m[:,df.columns.get_loc('Distance to TSS')]) < 2000)
    l1 = np.sum(mask)/len(df)

    df = pd.read_csv(hf2, sep='\t', header=0, index_col=False)
    m = df.as_matrix()
    ar = m[:,df.columns.get_loc('Distance to TSS')]
    ann_s = copy(m[:,df.columns.get_loc('Distance to TSS')])
    for k, v in ANN_DICT.iteritems(): ann_s[ar == k] = v
    mask    = np.logical_or(np.logical_and(ann_s != 'Intergenic', ann_s != ''), abs(m[:,df.columns.get_loc('Distance to TSS')]) < 2000)
    l2 = np.sum(mask)/len(df)

    res = funct(l1, l2)
    return res

def get_distance_matrix(meta_file,
                        funct           = None,
                        mask            = None,
                        directory       = None,
                        output_file_meta= None,
                        output_file_matrix= None,
                        only_selected   = False,
                        nthreads        = 1):

    logging.debug("Comuting distance matrix for metafile: {}".format(meta_file))

    if not funct:
        funct = jaccard_index
    meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)
    if mask:
        if isinstance(mask, dict):
            for k in mask.keys():
                if isinstance(mask[k], list):
                    meta = meta.loc[np.in1d(meta[k], mask[k]), :].copy(deep=True)
                else:
                    meta = meta.loc[meta[k]==mask[k], :].copy(deep=True)

        elif isinstance(mask, np.array):
            meta = meta.loc[mask,:].copy(deep=True)

    if only_selected:
        index_prev = meta.index
        meta = meta.reset_index(drop=True)

    if directory:
        meta['Local filename new'] = meta['Local filename'].apply(lambda x: os.path.abspath(directory) + '/' + x.split('/')[-1])
    else:
        meta['Local filename new'] = meta['Local filename']

    index = meta.index
    table = np.empty([max(index)+1, max(index)+1])
    table.fill(-1)
    logging.info("{}".format(len(meta)))
    #print(len(meta), meta.index)
    m = meta.as_matrix()
    ind = df.columns.get_loc('Local filename new')
    vals = m[:, ind]
    if nthreads==1:
        for ((i, row_i), (j, row_j)) in itertools.combinations(vals, 2):
            #debug.info("{}".format(i,j))
            res = compare_two_sets(row_i, row_j, funct)
            table[i, j] = res
            table[j, i] = res
    else:
        #LOCK = Lock()
        pool = Pool(nthreads)
        res = pool.map(parallel_compare, [(x, funct) for x in itertools.combinations(vals,2)])
        for val in res:
            table[val[0], val[1]] = val[2]
            table[val[1], val[0]] = val[2]

    if output_file_meta:
        meta.to_csv(output_file_meta, sep='\t', index=True)
        logging.debug("Resulting metafile written into: {}".format(output_file_meta))

    if output_file_matrix:
        np.savetxt(output_file_matrix, table, delimiter='\t', header='\t'.join([str(x) for x in index_prev]))
        logging.debug("Resulting matrix written into: {}".format(output_file_matrix))

    logging.debug("Computations are completed")

    return {'meta': meta, 'table': table}

LOCK = Lock()

def diff(a,b):
  return a-b

def parallel_compare(args):
    ((i, row_i), (j, row_j)), funct = args
    #logging.info("{} {}".format(i,j))
    res = compare_two_sets_prop(row_i, row_j, diff)
    return (i, j, res)
