##nice -n 20 python get_matrix.py mm H3K4me3 &> log_matrix_mm_H3K4me3.txt &

from gb_analysis.lib import *
from sys import argv
#mask        = {'Biosample organism': 'Drosophila melanogaster', 'Experiment target': 'H3K4me3'}
mode = argv[1]
if mode=='dm':
  sp = 'Drosophila melanogaster'
elif mode=='ce':
  sp = 'Caenorhabditis elegans'
elif mode=='hs':
  sp = 'Homo sapiens'
elif mode=='mm':
  sp = 'Mus musculus'

t = argv[2]

#sp          = 'Caenorhabditis elegans' #'Drosophila melanogaster' #'Caenorhabditis elegans'
sh_sp       = ''.join([i[0].lower() for i in sp.split()])
mask        = {'Biosample organism': sp, 'Experiment target': t}
#infile      = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/processed_data_all_1Sep.csv'
#outmeta     = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/full_{}_{}_meta_for_matrix.csv'.format(sh_sp, t)
#outmatrix   = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/full_{}_{}_matrix.csv'.format(sh_sp, t)
directory   = '../DATA/ENCODE_files_test_2'
infile      = '../DATA/processed_data_all_1Sep.csv'
outmeta     = '../DATA/full_{}_{}_meta_for_matrix2.csv'.format(sh_sp, t)
outmatrix   = '../DATA/full_{}_{}_matrix2.csv'.format(sh_sp, t)
get_distance_matrix(infile,
                    directory           = directory,
                    output_file_meta    = outmeta,
                    output_file_matrix  = outmatrix,
                    mask                = mask,
                    only_selected       = True,
                    nthreads            = 1
                    )
