from __future__ import division, print_function

import json
import requests

HEADERS = {'accept': 'application/json'}

def update_encode(out_metadata, out_dir, initial_meta=None, n_threads=1):

    pass

def download_metadata(URL=None, outfile=None):

    if URL==None:
        URL = "https://www.encodeproject.org/search/?type=experiment&frame=object&limit=all&format=json"

    # GET the search result
    response = requests.get(URL, headers=HEADERS)

    # Extract the JSON response as a python dict
    response_json_dict = response.json()

    # Print the object
    if outfile!=None:
        with open(outfile, 'w') as f:
            json.dump(response_json_dict, f, indent=4, separators=(',', ': '))

    return response_json_dict

def filter_meta_chip(meta, out=None):
    j = json.load(open(meta, 'r'))
    doi = [i for i in range(len(j["@graph"])) if j['@graph'][i]['assay_title']=='ChIP-seq']

    print(len(doi))

    out_json = []

    for i in doi[3:5]:
        print(i)
        exp_json = j['@graph'][doi[i]]
        for f in exp_json['files']:
            URL = 'https://www.encodeproject.org/'+f
            response = requests.get(URL, headers=HEADERS)
            file_json = response.json()
            if file_json['file_type'] == "bed narrowPeak" or file_json['file_type'] == "bed broadPeak":
                print('success')
                out_json.append(file_json)
                link = file_json["href"]
                filename = link.split('/')[-1]
                print('https://www.encodeproject.org'+link)
                r = requests.get('https://www.encodeproject.org'+link, allow_redirects=True, stream=True)
                with open(filename, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024):
                        if chunk: # filter out keep-alive new chunks
                            f.write(chunk)

    if out!=None:
        with open(out, 'w') as f:
            json.dump(json.dumps(out_json), f)

#>>> import pandas as pd
#>>> df = pd.DataFrame({'a':[1,2,3], 'b':[10,20,30]})
#>>> df.to_json(orient='records')
#'[{"a":1,"b":10},{"a":2,"b":20},{"a":3,"b":30}]'
