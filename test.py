from gbdb.gbdbup_class import DBParser

import numpy as np
import logging
logging.basicConfig(level=logging.DEBUG)

#reload(gb)
a = DBParser()
#a.dbLoadInfo(database='modENCODE', mode='FILE', filename='all_modencode.csv', format='csv', sep='\t', header=0, index_col=False, na_filter=False)
##a.dbParse('ex_out_modencode.txt', 1)
## a.dbDownload('./MODENCODE_files_test', 4, meta_file='ex_out_modencode.txt', partial=[0,30], db_type='MODENCODE')
##a._is_downloaded = True
#a.dbMerge('./MODENCODE_files_test', 1, partial=[0,5], meta_file="ex_out_modencode.txt", db_type='MODENCODE')
#a.dbProcessFiles(1, directory= './MODENCODE_files_test', partial=[0,5], db_type='MODENCODE', write_output_to='processed_files_me.csv')

# ENCODE
a.dbLoadInfo(database='ENCODE', mode='FILE', filename='all_encode.txt', format='json')
##a.dbParse('ex_out_encode.txt', 1, partial=[0,40])
#a.dbDownload('./ENCODE_files_test_ref', 1, meta_file="ex_out_encode.txt", db_type='ENCODE', partial=[0,7])
a._is_downloaded = True
a.dbMerge('./ENCODE_files_test', 1, meta_file="ex_out_encode.txt", db_type='ENCODE', partial=[0,7])

a.dbProcessFiles(1, directory= './ENCODE_files_test', db_type='ENCODE', write_output_to='processed_files.csv', partial=[0,5])