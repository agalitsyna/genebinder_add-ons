#!/usr/bin/env python2
# -*- coding: latin-1 -*-
'''GET the results of a search from an ENCODE server
Example run python search.py "https://www.encodeproject.org/search/?type=biosample&frame=object&limit=all&format=json" all_encode.txt
https://www.encodeproject.org/search/?type=experiment&frame=embedded&limit=all&format=json
'''

import requests, json
from sys import argv

# Force return from the server in JSON format
HEADERS = {'accept': 'application/json'}

# This searches the ENCODE database for the phrase "bone chip"
URL = argv[1]

# GET the search result
response = requests.get(URL, headers=HEADERS)

#with open(argv[2], 'wb') as f:
#        for chunk in response.iter_content(chunk_size=1024): 
#            if chunk: # filter out keep-alive new chunks
#                f.write(chunk)

# Extract the JSON response as a python dict
response_json_dict = response.json()

# Print the object
with open(argv[3], 'w') as f:
  json.dump(response_json_dict, f, indent=4, separators=(',', ': '))

