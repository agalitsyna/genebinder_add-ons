from gbdb.gbdbup_class import DBParser

import numpy as np
import logging
logging.basicConfig(level=logging.DEBUG)

initial = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/all_encode.txt'
directory = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/ENCODE_files_test'
further = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/ex_out_encode_1.txt'
final   = '/project/jgu-cbdm/andradeLab/scratch/agalitsy/processed_files_e.csv'
#reload(gb)
a = DBParser()
# ENCODE
a.dbLoadInfo(database='ENCODE', mode='FILE', filename=initial, format='json')
#a.dbParse(further, 4)
a.dbDownload(directory, 4, meta_file=further, db_type='ENCODE')
#a._is_downloaded = True
a.dbMerge(directory, 4, meta_file=further, db_type='ENCODE', write_output_to=final)

#a.dbProcessFiles(1, directory= directory, db_type='ENCODE', write_output_to=final, partial=[0,7])
