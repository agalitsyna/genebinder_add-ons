from __future__ import division, print_function

import pickle

import json
import requests
import urllib

import pandas as pd
import numpy as np
import os, subprocess
import sys
from multiprocessing import Pool, Lock
import shutil
import logging

import hashlib
from time import gmtime, strftime

from DBExceptions import *
from dbinfo_tool import *

HEADERS             = {'accept': 'application/json'}
ACCEPTED_FORMATS    = ['gff3', 'bed', 'gz', 'homer', 'homer_out']

meta_fields = ['File accession', 'File format',
               'Output type', 	'Experiment accession',
               'Assay',	'Biosample term id',
               'Biosample term name',	'Biosample type',
               'Biosample life stage',	'Biosample sex',
               'Biosample organism',	'Biosample treatments',
               'Biosample subcellular fraction term name',
               'Biosample phase',   'Biosample synchronization stage',
               'Experiment target',	'Antibody accession',
               'Library made from',	'Library depleted in',
               'Library extraction method',	'Library lysis method',
               'Library crosslinking method',	'Experiment date released',
               'Project', 'RBNS protein concentration',	'Library fragmentation method',
               'Library size range',	'Biosample Age',	'Biological replicate',
               'Technical replicate',	'Read length',	'Run type',
               'Paired end',	'Paired with',	'Derived from',
               'Size',	'Lab',	'md5sum',
               'File download URL',	'Assembly',	'Platform']

class DBParser():
    def __init__(self):

        self._is_loaded         = False
        self._is_db_set         = False
        self._is_parsed         = False
        self._is_filtered       = False
        self._is_downloaded     = False
        self._is_merged         = False
        self._is_processed      = False

    ### DATABASE UPDATE ###
    def load_meta_prev(self, filename):
        pass

    def load_fileinfo(self, filename):
        pass

    def check_files(self, directory):
        ## If experiment already uploaded, no need to check all files?
        pass

    ### DATABASE DOWNLOAD ###

    ### DATABASE METADATA DOWNLOAD/PARSER ###
    def dbLoadInfo(self, database=None, mode=None, filename=None, URL=None, format=None, **kwargs):
        '''
        Function for loading database info into basic class.
        User should specify either local file name, URL or provide function with object.
        Format should be also specified.
        :param mode: {'url', 'file'} str
        :param filename: str
        :param URL: str, full address
        :param obj: loaded object (e.g. via json.load())
        :param format: str, options: 'csv', 'json'
        :return:
        '''
        mode = mode.upper()
        format = format.upper()
        database = database.upper()

        if not mode in ['URL', 'FILE']:
            raise DBLoadError('Unsupported mode: {}'.format(mode))
        elif (mode=='URL' and URL==None) or (mode=='FILE' and filename==None):
            raise DBLoadError('With {0} mode input {0} should be specified'.format(mode))
        elif format==None:
            raise DBLoadError('Please, specify db info format.')

        try:
            if format=='JSON' and mode=='FILE':
                self._loaded_from = filename
                with open(filename, 'r') as f:
                    ret_json = json.load(f)
            elif format=='JSON' and mode=='URL':
                self._loaded_from = URL
                response = requests.get(URL, headers=HEADERS)
                ret_json = response.json()
            elif format=='CSV' and mode=='FILE':
                self._loaded_from = filename
                df = pd.read_csv(filename, **kwargs)
                ret_json = json.loads(df.to_json(orient='records'))
            elif format=='CSV' and mode=='URL':
                self._loaded_from = URL
                response = requests.get(URL)
                df = pd.read_csv(response, **kwargs)
                ret_json = json.loads(df.to_json(orient='records'))
            else:
                raise DBLoadError('Unknown input format: {} for load mode {}'.format(format, mode))
        except Exception as e:
            if not e.__name__=='DBLoadError':
                raise DBLoadError('Error: {} occured while loading information about database. Check compatibility of data input type and code.'.format(e.__name__))

        self._loaded_mode   = mode
        self._loaded_format = format

        if not database in ['ENCODE', 'MODENCODE']:
            raise DBLoadError('Unsupported database: {}'.format(database))
        else:
            self._dbSet(database)

        self._db_info = DBInfo(ret_json)
        self._is_loaded = True

        logging.info('DBInfo successfully loaded!')

    def _dbSet(self, t):
        self._db_type = t.upper()
        if t.upper() == 'ENCODE':
            self.URL_formatter = 'https://www.encodeproject.org/{}'
        elif t.upper() == 'MODENCODE':
            self.URL_formatter = 'ftp://data.modencode.org/{}'
            bck = np.array(['Cell-Line', 'Compound', 'Developmental-Stage', 'GrowthCondition',
                        'PCR-primer', 'RNAi-reagent', 'Strain', 'Temperature', 'Tissue',
                        'base pair', 'cellular-component', 'devstage', 'extract',
                        'organism part', 'quality', 'read-length', 'sampling-time-point',
                        'synthetic-RNA', 'temperature', 'threshold', 'whole-organism'],
                        dtype='|S19')
            if self._loaded_mode=='FILE' and self._loaded_mode=='csv':
                try:
                    a = pd.read_csv(self._loaded_from, sep='\t', header=0)
                    self._modencode_cond_keys = np.unique([x.split('=')[0] for x in '#'.join(a['Condition']).split('#')])###
                except:
                    self._modencode_cond_keys = bck
            else:
                self._modencode_cond_keys = bck

        self._is_db_set = True

    def dbParse(self, outfile=None, nthreads=5, clearFS=False, partial=None):
        '''
        Major parser function. Performs parallelized analysis of json, csv files with database info.
        In case of ENCODE database, internet connection is required.
        :param outfile: file to write output metadata
        :param clearFS: delete temporary file with all data or not
        :param nthreads: number of threads to use while parsing
        :param partial: list [n1, n2]: include data from n1:n2 elements of initial list -- it will be deprecated
        :return:
        '''

        if not self._is_loaded:
            raise DBLoadError('No database info loaded!')

        if not outfile:
            outfile = 'tmp_parse_{}_{}.csv'.format(self._db_type, strftime('%Y-%m-%d', gmtime()))

        with open(outfile, 'w') as f:
            self._db_meta = pd.DataFrame({k:[] for k in meta_fields})
            self._db_meta.to_csv(f, header=True, sep='\t', index=False, columns=meta_fields)

        args = self.get_all_args_meta(outfile)
        logging.debug('Full length of arguments to be parsed: {}'.format(len(args)))
        if isinstance(partial, list):
            args = args[partial[0] : partial[1]]
            logging.debug('Only part will be analysed: {}, {} in total'.format(len(args), partial))

        if nthreads == 1:
            logging.info('Entering single-thread mode of dbParser')
            for i in range(len(args)):
                parallel_parse_entry(args[i], parallel=False)

        else:
            logging.info('Entering multi-thread mode of dbParser ({} threads)'.format(nthreads))
            l = Lock()
            pool = Pool(nthreads, initializer=self._init_lock, initargs=(l,))
            pool.map(parallel_parse_entry, args)

        self._db_meta = pd.read_csv(outfile,  header=0, sep='\t', index_col=False)
        if clearFS:
            os.remove(outfile)

        self._is_parsed = True
        logging.info('Database info successfully parsed into database metadata!')

    @staticmethod
    def _init_lock(l):
        '''
        Litle trick for on-fly gloal lock creation.
        '''
        global lock
        lock = l

    def get_all_args_meta(self, outfile):
        '''
        Returns set of parameters to iterate through during parallelization.
        :param outfile:  metadata output file
        :param db: database type, ENCODE or modENCODE (case-insensitive)
        :return: args for parse_entry() function
        '''
        db = self._db_type

        if db.upper()=='ENCODE'.upper():
            return [ (DBInfo(x), outfile, db, i) for (i, x) in
                     enumerate(self._db_info.iterate_over_field('@graph').filter_by({'assay_title': 'ChIP-Seq'}))]
        elif db.upper()=='modencode'.upper():
            return [(DBInfo(x), outfile, db, i) for (i, x) in
                     enumerate(self._db_info.filter_by({'Technique':'CHIP-SEQ', 'File Format':'computed-peaks_gff3', 'Build': ['Dmel_r5.32', 'Cele_WS220']}))]
        else:
            raise DBParseError('Only ENCODE and modENCODE databases are currently supported')

    def dbFilter(self, file_meta=None, db=None, partial=None):
        '''
        Database filternig function.
        :param file_meta: optional, file to download metadata from (already parsed!)
        :return: indexes of _db_meta to load with no change and to be downloaded and merged
        '''

        if hasattr(self, '_db_meta') and not file_meta:
            logging.debug('Using metadata from previous analysis')
            db = self._db_type
            meta = self._db_meta
        elif hasattr(self, '_db_meta') and file_meta:
            logging.warning('Metadata already loaded and parsed from {}, it will be reloaded from file: {}'.format(
                self._loaded_from, file_meta))
            db = self._db_type
            meta = pd.read_csv(file_meta, sep='\t', header=0)
            self._db_meta = meta.copy(deep=True)
        elif file_meta and not hasattr(self, '_db_meta'):
            logging.debug('Loading meta file: {}'.format(file_meta))
            if not db:
                if not hasattr(self, '_db_type'):
                    raise DBFilterError('db type is required.')
                else:
                    db = self._db_type

            meta = pd.read_csv(file_meta, sep='\t', header=0)

        else:
            raise DBFilterError("No metadata provided or loaded!")

        if isinstance(partial, list):
            logging.debug("Partial filtering, Using information from {} to {}".format(*partial))
            meta = meta.loc[partial[0]:partial[1]]

        if self._db_type=='MODENCODE':

            tmp = meta.index

            #self._filtered   = np.array(tmp).tolist()
            #self._for_merge  = np.array([]).tolist()

            l = len(meta)

            ind_filtered    = []
            ind_for_merge   = []

            g = meta.groupby('Experiment accession')
            exps = np.unique(meta['Experiment accession'])

            for exp in exps:
                cur = g.get_group(exp)

                if len(cur) == 1:
                    ind_filtered.extend(cur.index)
                else:
                    added = 0

                    if len(np.unique(cur['Experiment target']))>1:
                        raise DBFilterError('Multiple factors for single experiment. Check for error in experiment: {}'.format(exp))

                    ind_combined    = [i for i, r in cur.iterrows() if 'combined' in r['File download URL'] ]
                    ind_repset      = [i for i, r in cur.iterrows() if 'repset' in r['File download URL'] ]

                    if len(ind_combined)>0 and len(ind_repset)>0:
                        logging.warning("!!! Both combined and repset are present in modencode filename... Appending only combined into filter")

                    if len(ind_combined)==1:
                        added = 1
                        l -= len(cur) - 1
                        ind_filtered.append(ind_combined[0])
                        continue

                    if len(ind_repset)==1:
                        added = 1
                        l -= len(cur) - 1
                        ind_filtered.append(ind_repset[0])
                        continue

                    if len(ind_repset)+len(ind_combined)==0:
                        added = 1
                        ind_for_merge.extend(cur.index)
                        continue

                    if not added:
                        raise DBFilterError("modENCODE filtering error!")

            logging.debug("Filtered: {} files; For merge: {} files".format(len(ind_filtered), len(ind_for_merge)))
            logging.debug("Index filtered and for merge intersection (should be 0): {}".format(len(np.intersect1d(ind_filtered, ind_for_merge))))
            logging.debug("Index filtered and for merge union (should be {}): {}".format(l, len(np.union1d(ind_filtered, ind_for_merge))))

            self._filtered   = list(ind_filtered)
            self._for_merge  = list(ind_for_merge)

            self._is_filtered = True

            return {'filtered':self._filtered, 'for_merge':self._for_merge}

        elif self._db_type=='ENCODE':

            logging.debug('---> {}\tinitial number of files'.format(len(meta)))
            meta = meta[meta['Output type'] != 'hotspots']
            logging.debug('---> {}\tafter removing hotspot files'.format(len(meta)))

            l = len(meta)

            ind_filtered    = []
            ind_for_merge   = []

            priority_output_type   = ['optimal idr thresholded peaks', 'conservative idr thresholded peaks', 'replicated peaks', 'peaks']
            dict_organism_assembly_order = {'Drosophila melanogaster': ['dm3', 'dm6'],
                                            'Caenorhabditis elegans': ['ce10', 'ce11'],
                                            'Homo sapiens':['hg19', 'hg38'],
                                            'Mus musculus':['mm10', 'mm9', 'mm8']}

            g = meta.groupby('Experiment accession')
            exps = np.unique(meta['Experiment accession'])

            for exp in exps:
                cur = g.get_group(exp)

                if len(cur) == 1:
                    ind_filtered = np.append(ind_filtered, cur.index)
                else:
                    tmp_b = cur['Biological replicate']
                    all_b = np.unique(' '.join(tmp_b).replace('[','').replace(']',''))
                    #max_l = len(all_b)
                    #tmp_t = cur['Technical replicate']
                    #all_t = np.unique(' '.join(tmp_t).replace('[','').replace(']',''))
                    added = 0
                    for pr in priority_output_type: # check for single optimal, conservative or replicated peak file
                        if np.sum(cur['Output type']==pr)==1:
                            added = 1
                            ind_filtered = np.append( ind_filtered, cur[cur['Output type']==pr].index )
                            l -= len(cur[cur['Output type']!=pr])
                            break
                    if not added:
                        # no single merged file found
                        #logging.info("\t\tFiltering: not added with one iteration")

                        rep_n = tmp_b.apply(json.loads).apply(np.unique).apply(len)
                        #logging.info("\t\trep_n: {}".format(rep_n))

                        #rep_b = tmp_b.apply(json.loads)

                        if np.all(rep_n==1):       # no merged file at all, add everything to further merger
                            ind_for_merge = np.append( ind_for_merge, cur.index )
                        elif np.all(rep_n<=1):     # no merged file, but strange file with no annotation present, treat as previous
                            ind_for_merge = np.append( ind_for_merge, cur[rep_n==1].index )
                            l -= len(cur[rep_n!=1])
                        else:                      # more complicated cases, e.g. files for several assemblies available
                            added = 0
                            for key_org in dict_organism_assembly_order.keys():
                                if np.all(cur['Biosample organism']==key_org):
                                    for ga in dict_organism_assembly_order[key_org]:
                                        for pr in priority_output_type:
                                            if np.sum(np.logical_and(cur['Output type']==pr, cur['Assembly']==ga))==1:
                                                added=1
                                                ind_filtered = np.append( ind_filtered, cur[np.logical_and(cur['Output type']==pr, cur['Assembly']==ga)].index )
                                                l -= len(cur) - 1
                                                break
                                        if added==1:
                                            break

                                    if added==0:
                                        raise DBFilterError('Error for species: {}'.format(cur))#['Biosample organism']))
                            if added == 0:
                                raise DBFilterError('No oragnism found: {}'.format(cur['Biosample organism']))

            logging.debug("Filtered: {} files; For merge: {} files".format(len(ind_filtered), len(ind_for_merge)))
            logging.debug("Index filtered and for merge intersection (should be 0): {}".format(len(np.intersect1d(ind_filtered, ind_for_merge))))
            logging.debug("Index filtered and for merge union (should be {}): {}".format(l, len(np.union1d(ind_filtered, ind_for_merge))))

            self._filtered   = list(ind_filtered)
            self._for_merge  = list(ind_for_merge)

            self._is_filtered = True

            return {'filtered':ind_filtered, 'for_merge':ind_for_merge}

        else:
            raise DBFilterError("Unknown database type: {}".format(self._db_type))


    def dbDownload(self, outdir, nthreads,
                   meta_file=None, db_type=None,
                   partial=None, filter=None,
                   overwrite_filter=True, check_sum=True,
                   write_output=None,
                   virtual = False):

        if meta_file==None:
            if not hasattr(self, '_db_meta'):
                raise DBDownloadError('No prepared metadata provided')
        else:
            if hasattr(self, '_db_meta'):
                logging.warning('Metadata already loaded. Reloading...')
            self._db_meta = pd.read_csv(meta_file, header=0, sep='\t', index_col=False)
            self._db_type = db_type

        outdir = os.path.abspath(outdir) + '/'
        self._filedir = outdir
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        if not self._is_filtered and not filter:
            filter = self.dbFilter(partial=partial)
        elif self._is_filtered and not filter:
            filter = { 'filtered':  list(self._filtered),
                       'for_merge': list(self._for_merge) }
        elif self._is_filtered and filter:
            if overwrite_filter:
                logging.warning('Filter already present: {} (filtered) + {} (for merge) = {} files in total.\nIt will be overwritten: {} + {} = {}'.format(
                        len(self._filtered), len(self._for_merge), len(self._filtered) + len(self._for_merge),
                        len(filter['filtered']), len(filter['for_merge']), len(filter['filtered']) + len(filter['for_merge'])  ))
                self._filtered = filter['filtered']
                self._for_merge = filter['for_merge']
            else:
                logging.warning('Actual filter: {} (filtered) + {} (for merge) = {} files in total.\nApplying another filter: {} + {} = {}'.format(
                        len(self._filtered), len(self._for_merge), len(self._filtered) + len(self._for_merge),
                        len(filter['filtered']), len(filter['for_merge']), len(filter['filtered']) + len(filter['for_merge'])  ))

        filter['full'] = np.append( filter['filtered'], filter['for_merge'] )
        logging.debug("Filter: {}".format(filter))

        links   = self._db_meta.loc[filter['full'], 'File download URL'].reset_index(drop=True)
        md5sums = self._db_meta.loc[filter['full'], 'md5sum'].reset_index(drop=True)

        #if isinstance(partial, list):
        #    links       = links[partial[0] : partial[1]]
        #    md5sums     = md5sums[partial[0] : partial[1]]

        args = [(link, outdir + link.split('/')[-1], self._db_type, check_sum, md5) for link, md5 in zip(links, md5sums)]

        if not virtual:
            logging.info("Starting download into directory: {}".format(outdir))
            if nthreads == 1:  # Sequential variant of run
                for x in args:
                    download_file_from_link(x)

            else:  #parallel variant of run
                pool = Pool(nthreads)
                pool.map(download_file_from_link, args)

        meta = self._db_meta.loc[filter['full']]
        meta['Local filename'] = self._db_meta.loc[filter['full']]['File download URL'].apply(lambda x: self._filedir + x.split('/')[-1])

        #new_meta        = self._rename_by_exp(filter['filtered'])
        #additional_meta = self._merge(filter['for_merge'])

        #meta = pd.concat([new_meta, additional_meta])
        self._db_meta = meta.copy(deep=True)
        self._is_downloaded = True

        if write_output:
            self._db_meta.to_csv(write_output, sep='\t', index=False)

    def dbMerge(self, outdir, nthreads,
                meta_file   =   None,
                db_type     =   None,
                partial     =   None,
                filter      =   None,
                overwrite_filter=True,
                write_output_to=None,
                virtual     =   False):

        if not self._is_downloaded:
            raise DBMergeError("Files are not downloaded or mode _donloaded is not set")
        if meta_file==None:
            if not hasattr(self, '_db_meta'):
                raise DBDownloadError('No prepared metadata provided')
        else:
            if hasattr(self, '_db_meta'):
                logging.warning('Metadata already loaded. Reloading...')
            self._db_meta = pd.read_csv(meta_file, header=0, sep='\t', index_col=False)
            self._db_type = db_type

        if outdir:
            outdir = os.path.abspath(outdir) + '/'
            self._filedir = outdir
        else:
            outdir = self._filedir
        if not os.path.exists(outdir):
            raise DBMergeError("Directory with files not found: {}".format(outdir))

        if not self._is_filtered and not filter:
            filter = self.dbFilter(partial=partial)
        elif self._is_filtered and not filter:
            filter = { 'filtered':list(self._filtered),
                       'for_merge':list(self._for_merge) }
        elif self._is_filtered and filter:
            if overwrite_filter:
                logging.warning('Filter already present: {} (filtered) + {} (for merge) = {} files in total.\nIt will be overwritten: {} + {} = {}'.format(
                        len(self._filtered), len(self._for_merge), len(self._filtered) + len(self._for_merge),
                        len(filter['filtered']), len(filter['for_merge']), len(filter['filtered']) + len(filter['for_merge'])  ))
                self._filtered = filter['filtered']
                self._for_merge = filter['for_merge']
            else:
                logging.warning('Actual filter: {} (filtered) + {} (for merge) = {} files in total.\nApplying another filter: {} + {} = {}'.format(
                        len(self._filtered), len(self._for_merge), len(self._filtered) + len(self._for_merge),
                        len(filter['filtered']), len(filter['for_merge']), len(filter['filtered']) + len(filter['for_merge'])  ))

        filter['full'] = np.append( filter['filtered'], filter['for_merge'] )
        logging.debug('Resulting filter (total len {}): {}'.format(len(filter['full']), filter))

        ###new_meta = self._db_meta.loc[filter['full']].reset_index(drop=True) #TODO: accession as filename, TODO: modencode pass
        new_meta        = self._rename_by_exp(filter['filtered'], virtual=virtual)
        additional_meta = self._merge(filter['for_merge'], virtual=virtual)
        meta            = pd.concat([new_meta, additional_meta])

        #self._db_meta.to_csv('01_before filtering.csv', sep='\t')
        self._db_meta = meta.copy(deep=True)
        #self._db_meta.to_csv('01_after filtering.csv', sep='\t')
        logging.debug('Resulting meta length: {}'.format(len(self._db_meta)))
        self._is_merged = True

        if write_output_to:
            logging.info("Writing resulting dateframe into: {}".format(write_output_to))
            meta.to_csv(write_output_to, sep='\t', index=False, columns=meta_fields+['Local filename'])

    ### Processing: gff/bed/gz conversion, HOMER run and so on.
    def dbProcessFiles(self, nthreads,
                       meta_file    =   None,
                       directory    =   None,
                       partial      =   None,
                       db_type      =   None,
                       write_output_to= None,
                       virtual      =   False):

        if meta_file==None:
            if not hasattr(self, '_db_meta'):
                raise DBProcessError('No prepared metadata provided')
        else:
            if hasattr(self, '_db_meta'):
                logging.warning('Metadata already loaded. Reloading...')
            self._db_meta = pd.read_csv(meta_file, header=0, sep='\t', index_col=False)
            self._db_type = db_type

        if not directory:
            if not hasattr(self, '_filedir'):
                raise DBProcessError('No directory with downloaded files provided')
        else:
            directory = os.path.abspath(directory) + '/'
            if not os.path.exists(directory) or os.listdir(directory) == []:
                raise DBProcessError('Files directory does not exist or is empty')
            if hasattr(self, '_filedir'):
                logging.warning('File directory was set to {}, now is: {}'.format(self._filedir, directory))
            self._filedir = directory

        links               = self._db_meta['Local filename'].values
        genome_assemblies   = self._db_meta['Assembly'].values
        meta                = self._db_meta
        if isinstance(partial, list):
            links = links[partial[0] : partial[1]]
            genome_assemblies = genome_assemblies[partial[0] : partial[1]]
            logging.info("Entering partial process mode. Files to be processed: {}".format(len(links)))
            meta = meta.loc[partial[0]:partial[1]]

        if not virtual:
            if nthreads == 1:
                for i in range(len(links)):
                    link = links[i]
                    genome_assembly = genome_assemblies[i]
                    logging.debug('Processing: %s\t%s', link, genome_assembly)
                    link_file = directory + link.split('/')[-1]
                    res = FileProcessor(link_file, genome_assembly).run_pipeline()
                    logging.info('Status {} for processing link: {}'.format(res, link_file))

            else:
                pool = Pool(nthreads)

                args = [(directory + links[i].split('/')[-1], genome_assemblies[i]) for i in range(len(links))]

                pool.map(parallel_process_files, args)

        meta.loc[:, 'Local filename'] = meta['Local filename'].apply(lambda x: x.split('.')[0]+'.homer_out')

        if write_output_to:
            logging.info("Writing resulting dataframe into: {}".format(write_output_to))
            meta.to_csv(write_output_to, sep='\t', index=False, columns=meta_fields+['Local filename'])

    def dbCheckDownloaded(self, meta_file=None, filedir=None, write_absent=None, write_present=None):

        logging.info('Check for files downloaded started')

        if not hasattr(self, '_db_meta'):
            if not meta_file:
                raise DBCheckError("No metadata to check provided!")
            else:
                self._db_meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)
        else:
            if meta_file:
                logging.warning("Metadata already loaded/parsed, rewriting it")
                self._db_meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)

        if not hasattr(self, '_filedir'):
            if not filedir:
                raise DBCheckError("No filedir provided!")
            else:
                self._filedir = os.path.abspath(filedir)+'/'
        else:
            if filedir:
                logging.warning("Filedir already set, rewriting it")
                self._filedir = os.path.abspath(filedir)+'/'

        if not 'Local filename' in self._db_meta.keys():
            logging.info('No local filename field in metadata, computing...')
            self._db_meta['Local filename'] = self._db_meta.apply(
                lambda x: self._filedir + x['File download URL'].split('/')[-1], axis=1)

        files = self._db_meta['Local filename']
        absent_ind = []
        for ind, file in zip(self._db_meta.index.values, files.values):
            if not os.path.isfile(file):
                absent_ind.append(ind)

        absent = self._db_meta.loc[absent_ind]
        if len(absent)!=0:
            logging.error("CheckDownload failed. Writing absent data into {}".format(write_absent))
            absent.to_csv(write_absent, sep='\t', index=False)

        present_ind = [i for i in self._db_meta.index if i not in absent_ind]
        logging.info("Check Downloaded finished. Number of good files: {}.".format(len(present_ind)))
        if write_present:
            logging.info("Writing good results into: {}".format(write_present))
            present = self._db_meta.loc[present_ind]
            present.to_csv(write_present, sep='\t', index=False)

    def dbCheckMerged(self, meta_file=None, filedir=None, write_absent=None, write_present=None):
        logging.info('Check for files merged started')

        if not hasattr(self, '_db_meta'):
            if not meta_file:
                raise DBCheckError("No metadata to check provided!")
            else:
                self._db_meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)
        else:
            if meta_file:
                logging.warning("Metadata already loaded/parsed, rewriting it")
                self._db_meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)

        if not hasattr(self, '_filedir'):
            if not filedir:
                raise DBCheckError("No filedir provided!")
            else:
                self._filedir = os.path.abspath(filedir)+'/'
        else:
            if filedir:
                logging.warning("Filedir already set, rewriting it")
                self._filedir = os.path.abspath(filedir)+'/'

        if not 'Local filename' in self._db_meta.keys():
            logging.info('No local filename field in metadata, computing...')
            filter = self.dbFilter()
            self._db_meta = pd.concat(
                [self._merge(filter['for_merge'], virtual=True),
                 self._rename_by_exp(self._merge(filter['filtered'], virtual=True))])

        files = self._db_meta['Local filename']
        absent_ind = []
        for ind, file in zip(self._db_meta.index.values, files.values):
            if not os.path.isfile(file):
                absent_ind.append(ind)

        absent = self._db_meta.loc[absent_ind]
        if len(absent)!=0:
            logging.error("CheckMerged failed. Writing absent data into {}".format(write_absent))
            absent.to_csv(write_absent, sep='\t', index=False)

        present_ind = [i for i in self._db_meta.index if i not in absent_ind]
        logging.info("Check Merged finished. Number of good files: {}.".format(len(present_ind)))
        if write_present:
            logging.info("Writing good results into: {}".format(write_present))
            present = self._db_meta.loc[present_ind]
            present.to_csv(write_present, sep='\t', index=False)

    def dbCheckProcessed(self, meta_file=None, filedir=None, write_absent=None, write_present=None):

        logging.info('Check for processed files started')

        if not hasattr(self, '_db_meta'):
            if not meta_file:
                raise DBCheckError("No metadata to check provided!")
            else:
                self._db_meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)
        else:
            if meta_file:
                logging.warning("Metadata already loaded/parsed, rewriting it")
                self._db_meta = pd.read_csv(meta_file, sep='\t', header=0, index_col=False)

        if not hasattr(self, '_filedir'):
            if not filedir:
                raise DBCheckError("No filedir provided!")
            else:
                self._filedir = os.path.abspath(filedir)+'/'
        else:
            if filedir:
                logging.warning("Filedir already set, rewriting it")
                self._filedir = os.path.abspath(filedir)+'/'

        if not 'Local filename' in self._db_meta.keys():
            logging.info('No local filename field in metadata, computing...')
            filter = self.dbFilter()
            self._db_meta = pd.concat(
                [self._merge(filter['for_merge'], virtual=True),
                 self._rename_by_exp(self._merge(filter['filtered'], virtual=True))])

        files = self._db_meta['Local filename']
        absent_ind = []
        for ind, file in zip(self._db_meta.index.values, files.values):
            if not os.path.isfile(file):
                absent_ind.append(ind)
                continue
            with open(file, 'r') as inf:
                f = inf.readlines()
            if len(f)<2:
                absent_ind.append(ind)
                continue
            splited = np.array([np.array(f[i].split()[6:]) for i in range(min(10,len(f)))])
            if np.sum(splited=='NA')>(3*min(10,len(f))):
                absent_ind.append(ind)
                continue

        absent = self._db_meta.loc[absent_ind]
        if len(absent)!=0:
            logging.error("CheckProcessed failed. Writing absent data into {}".format(write_absent))
            absent.to_csv(write_absent, sep='\t', index=False)
        present_ind = [i for i in self._db_meta.index if i not in absent_ind]
        logging.info("Check Processed finished. Number of good files: {}.".format(len(present_ind)))
        if write_present:
            logging.info("Writing good results into: {}".format(write_present))
            present = self._db_meta.loc[present_ind]
            present.to_csv(write_present, sep='\t', index=False)

    def dbCleanFolder(self):
        pass

    def _rename_by_exp(self, filter, virtual=False):

        logging.debug("Files to rename: {}".format(len(filter)))

        #logging.debug("{}".format(filter))
        #logging.debug("{}".format(self._db_meta.loc[filter]))
        tmp_meta    = self._db_meta.loc[filter].reset_index(drop=True)
        #logging.debug("{}".format(tmp_meta))
        if self._db_type == 'ENCODE':
            tmp_f       = tmp_meta.apply( lambda x: self._filedir + x['File download URL'].split('/')[-1], axis=1)#x['File accession'] + '.' + '.'.join(x['File download URL'].split('/')[-1].split('.')[1:]), axis=1)
            tmp_t       = tmp_meta.apply( lambda x: self._filedir + x['Experiment accession'].split('/')[2] + '.' + '.'.join(x['File download URL'].split('/')[-1].split('.')[1:]), axis=1)
        elif self._db_type=='MODENCODE':
            tmp_f       = tmp_meta.apply( lambda x: self._filedir + x['File download URL'].split('/')[-1], axis=1)
            tmp_t       = tmp_meta.apply( lambda x: self._filedir + str(x['File accession']) + '.' + '.'.join(np.unique([i.lower() for i in x['File download URL'].split('/')[-1].split('.')[1:] if i.lower() in ACCEPTED_FORMATS ])), axis=1)

        if self._db_type=='ENCODE':
            tmp_meta['File accession']  = tmp_meta['Experiment accession'].apply(str)
        elif self._db_type=='MODENCODE':
            tmp_meta['File accession']  = tmp_meta['File accession']

        tmp_meta['Size']            = pd.Series(np.repeat('', len(tmp_meta)))
        tmp_meta['md5sum']          = pd.Series(np.repeat('', len(tmp_meta)))
        tmp_meta['Local filename']  = pd.Series(tmp_t)

        if not virtual:
            for f, t in zip(tmp_f, tmp_t):
                logging.debug("Renaming: {} to {}".format(f,t))
                shutil.move(f, t)

            logging.debug("Renaming done")
        else:
            logging.debug("Virtual renaming done. Files untouched, metadata modified.")
        return tmp_meta

    def _merge(self, filter, nthreads=1, virtual=False):

        try:
            tmp = subprocess.check_call('bedtools', stdout=subprocess.PIPE)
        except Exception as e:
            raise DBMergeError("Bedtools are not supported on this machine.")
        del tmp

        g = self._db_meta.loc[filter].groupby('Experiment accession')
        exps = np.unique(self._db_meta.loc[filter]['Experiment accession'])

        args = []
        new_meta = pd.DataFrame(columns=self._db_meta.columns)
        new_meta['Local filename'] = pd.Series()
        for ind, exp in enumerate(exps):
            cur     = g.get_group(exp).reset_index(drop=True)
            if self._db_type.upper()=='ENCODE':
                tmp_f = np.array(cur['File accession'])
                tmp_fe = '.' + '.'.join(
                    np.array(cur['File download URL'])[0].split('/')[-1].split('.')[1:])
                f1 = self._filedir  + tmp_f[0]  + tmp_fe
                f2 = [self._filedir + tmp_f_    + tmp_fe for tmp_f_ in tmp_f[1:]]
                expacc  = exp.split('/')[2]
            elif self._db_type.upper()=='MODENCODE':
                ### !!!
                tmp_f = np.array(cur.apply(lambda x: self._filedir + x['File download URL'].split('/')[-1], axis=1))
                f1 = tmp_f[0]
                f2 = tmp_f[1:]
                expacc  = 'MOD'+str(exp)
            else:
                raise DBMergeError("unsupported database type: {}".format(self._db_type))
            outf    = self._filedir + '{}.bed'.format(expacc)
            if len(tmp_f)>1:
                tmp_meta = cur.loc[0].copy(deep=True)
                tmp_meta['File accession']      = expacc
                tmp_meta['File download URL']   = ','.join(cur['File download URL'])
                tmp_meta['md5sum']              = ''
                tmp_meta['Size']                = ''
                tmp_meta['Local filename']      = outf
                new_meta.loc[ind]=tmp_meta
            else:
                raise DBDownloadError("Erroneous filtering, one file per experiment")
            args.append( (f1, f2, outf) )

        logging.debug("Number of merge actions to be performed: {}".format(len(args)))

        if not virtual:
            if nthreads==1:
                logging.info("Entering single thread mode for merge")
                for x in args:
                    merge_files(x, parallel=False)
            else:
                logging.info("Entering {}-thread mode for merge".format(len(args)))

                pool = Pool(nthreads)
                pool.map(merge_files, args)

            self._is_merged = True

        else:
            logging.debug("Virtual merge done. Files untouched, metadata modified")
        #new_meta['md5sum'] = new_meta.apply(lambda x: hashlib.md5(open().read()).hexdigest() , axis=1)

        return new_meta


#@staticmethod
def parallel_parse_entry(args, parallel=True):
    '''
    Function to run database entries parser in parallel.
    Writes result into file.
    Required for DBParser class.
    :param args: args returned by DBParser.get_all_args()
    :return:
    '''
    # TODO: find a way to include this function as class method.
    # TODO: add saving into dataframe attribute for DBParser, writing into file -- only option

    (dbinfo, outfile, db, index) =  args
    logging.info('Parsing entry {}'.format(index))

    try:
        if db=='ENCODE':

            entrylist = dbinfo\
                .iterate_over_field('files').apply(action='download_json', URL_formatter='https://www.encodeproject.org{}')\
                .filter_by({'file_type': ['bed narrowPeak', 'bed broadPeak']})

            l = len(entrylist)
            if l==0:
                return 0

            logging.debug('Files to be downloaded: {}'.format(l))

            meta = {k:[None for x in range(l)] for k in meta_fields}

            meta_dict = {'File accession':'accession', 'File format':'file_type',
                           'Output type': 'output_type', 	'Experiment accession':'dataset',
                           'Size': 'file_size',	'md5sum': 'content_md5sum',
                         'Biological replicate': 'biological_replicates', 'Technical replicate': 'technical_replicates',
                         'Experiment date released': 'date_released'}  ##TODO change to date_created?

            for field_to in meta_dict.keys():
                field_from = meta_dict[field_to]
                meta[field_to] = entrylist.get_field(field_from).stringify()

            meta['Lab'] = entrylist.get_field('lab').get_field('title').stringify()
            meta['File download URL'] = entrylist.get_field('href').apply(function=lambda x: 'https://www.encodeproject.org{}'.format(x))

            def mod_org(x):
                if x in ['mm8', 'mm9', 'mm10', 'hg19', 'hg38', 'dm3', 'dm6', 'ce10', 'ce11']:
                    return x
                elif x=='mm10-minimal':
                    return 'mm10'
                elif x=='GRCh38':
                    return 'hg38'
                else:
                    raise DBParseError('Yet unknown assembly: {}'.format(x))

            meta['Assembly'] = entrylist.get_field('assembly').apply(function=mod_org )

            explist = entrylist.get_field('dataset')
            exp = np.unique(explist._loaded)
            if len(exp) <> 1:
                logging.error('Multiple experiments for experiment ?')
            else:
                explist = entrylist.get_field('dataset').loc(0).apply(action='download_json', URL_formatter='https://www.encodeproject.org{}')

            meta1 = {}
            meta1['Experiment target']   = explist.get_field('target').get_field('label').stringify()
            meta1['Biosample organism']  = explist.get_field('target').get_field('organism').get_field('scientific_name').stringify()
            meta1['Biosample term id']   = explist.get_field('biosample_term_id').stringify()
            meta1['Biosample term name'] = explist.get_field('biosample_term_name').stringify()

            replicates = explist.get_field('replicates')
            meta1['Biosample treatments'] = explist.get_field('replicates').loc(0).get_field('library').get_field('biosample').get_field('treatments').get_field('treatment_term_id').stringify(hard=True)
            meta1['Antibody accession']   = explist.get_field('replicates').loc(0).get_field('antibody').get_field('accession').stringify()
            meta1['Biosample sex']        = explist.get_field('replicates').loc(0).get_field('library').get_field('biosample').get_field('sex').stringify()

            for k in meta1.keys():
                meta[k] = [meta1[k] for x in range(len(meta['File download URL']))]

            meta = pd.DataFrame(meta)

            if parallel==True:
                with lock:
                    with open(outfile, 'a') as f:
                        meta.to_csv(f, header=False, sep='\t', index=False, quoting=False, columns=meta_fields)
            else:
                with open(outfile, 'a') as f:
                    meta.to_csv(f, header=False, sep='\t', index=False, quoting=False, columns=meta_fields)


        elif db.upper() == 'modENCODE'.upper():

            meta = {k:[None] for k in meta_fields}

            meta_dict = {'Lab':'Principal Investigator', 'File format': 'File Format',
                   'Biosample life stage': 'Stage',
                   'Biosample organism': 'Organism',
                   'Experiment target': 'Factor',	'Biological replicate': 'ReplicateSetNum'}

            for field_to in meta_dict.keys():
                field_from = meta_dict[field_to]
                meta[field_to] = [dbinfo.get_field(field_from).stringify()]

            meta['File download URL'] = 'ftp://data.modencode.org/' + \
                                        dbinfo.get_field('Directory').stringify()[:-3] + '/' + \
                                        dbinfo.get_field('Uniform filename').stringify().replace('#', '%23').replace(':','%3A')

            meta['File accession'] = 'MOD'+dbinfo.get_field('DCC id').stringify()
            ### !!!
            #meta['File accession']          = dbinfo.get_field('Uniform filename').stringify().replace('#', '%23').replace(':','%3A')
            meta['Experiment accession']    = dbinfo.get_field('DCC id').stringify()

            meta_cond = {k.split('=')[0]:k.split('=')[1] for k in dbinfo.get_field('Condition').stringify().split('#')}
            m = {'Biosample term name':    ['Cell-line', 'Strain', 'Tissue', 'organism part'],
                 'Biosample treatments':    ['Compound', 'GrowthCondition', 'RNAi-reagent', 'temperature', 'Temperature'],
                 'Biosample life stage':    ['Developmental-Stage', 'devstage'],
                 'Read length':             ['read-length']}

            for k in m.keys():
                res = []
                for v in m[k]:
                    if v in meta_cond.keys():
                        res.append(meta_cond[v])
                meta[k] = ','.join(res)

            m_build = dbinfo.get_field('Build').stringify()

            if m_build == 'Dmel_r5.32':
                meta['Assembly'] = 'dm3'
            elif m_build == 'Cele_WS220':
                meta['Assembly'] = 'ce10'
            else:
                #meta['Assembly'] = m_build
                raise DBParseError('Unsupported genome for modENCODE: {}'.format(m_build))

            meta = pd.DataFrame(meta)

            if parallel== True:
                with lock:
                    with open(outfile, 'a') as f:
                        meta.to_csv(f, header=False, sep='\t', index=False, quoting=False, columns=meta_fields)
            else:
                with open(outfile, 'a') as f:
                    meta.to_csv(f, header=False, sep='\t', index=False, quoting=False, columns=meta_fields)


        else:
            raise DBParseError('Only ENCODE and modENCODE databases are currently supported')

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        logging.error("{}".format(e))

def download_file_from_link(args):
    (link, filename, db_type, check_sum, md5) = args #link, outdir + link.split('/')[-1], self._db_type

    if db_type == 'ENCODE':

            if check_sum:
                trials = 5
                while not trials-5:
                    r = requests.get(link, allow_redirects=True, stream=True)
                    with open(filename, 'wb') as f:
                        for chunk in r.iter_content(chunk_size=1024):
                            if chunk:
                                f.write(chunk)
                        md5_actual = hashlib.md5(open(filename).read()).hexdigest()
                        if not md5==md5_actual:
                            break
                        else:
                            trials -= 1
                if trials==0:
                    logging.error('Md5sum for file {} not achieved after 5 trials (link: {}).'.format(filename, link))
            else:
                r = requests.get(link, allow_redirects=True, stream=True)
                with open(filename, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1024):
                        if chunk:
                            f.write(chunk)

    elif db_type == 'MODENCODE':

            if check_sum:
                trials = 5
                while not trials-5:
                    urllib.urlretrieve(link, filename)
                    md5_actual = hashlib.md5(open(filename).read()).hexdigest()
                    if not md5==md5_actual:
                        break
                    else:
                        trials -= 1
                if trials==0:
                    logging.error('Md5sum for file {} not achieved after 5 trials (link: {}).'.format(filename, link))
            else:
                urllib.urlretrieve(link, filename)

def merge_files(args, parallel=True):

    logging.info('MERGE')
    FileProcessor(args[0]).file2bed()
    for i in range(len(args[1])):
        FileProcessor(args[1][i]).file2bed()

    outname = args[2]

    a1 = '.'.join([i for i in args[0].split('.') if not i.lower() in ACCEPTED_FORMATS])+'.bed'
    a2 = ' '.join(['.'.join([i for i in a.split('.') if not i.lower() in ACCEPTED_FORMATS])+'.bed' for a in args[1]])
    command = 'bedtools intersect -wa -wb -a {} -b {} > {}'.format(a1, a2, outname).replace('(', '\(').replace(')', '\)').replace('=', '\=')
    logging.info("Running: {}".format(command))
    proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (stdout, stderr) = proc.communicate()
    if stderr:
        logging.error("File merge failed for {}: intersection of {} and {}".format(outname, a1, a2))
    else:
        return 0
    #subprocess.call(command, shell=True)


def parallel_process_files(args):
    link_file, genome_assembly = args
    res = FileProcessor(link_file, genome_assembly).run_pipeline()
    logging.debug('Status {} for processing link: {}'.format(res, link_file))
