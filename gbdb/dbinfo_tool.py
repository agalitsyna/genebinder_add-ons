from __future__ import division, print_function

import json
import requests
#import gzip
import subprocess
import os
import logging

from DBExceptions import *

HEADERS = {'accept': 'application/json'}

class DBInfo():

    def __init__(self, data): ### TODO add possibility to save and download pandas DataFrames?

        if isinstance(data, dict):
            self._mode      = 'dict'
            self._data      = json.dumps(data)
            self._loaded    = self._load()
        elif isinstance(data, list):
            self._mode      = 'list'
            self._data      = json.dumps(data)
            self._loaded    = self._load()
        elif isinstance(data, str) or isinstance(data, unicode) or isinstance(data, int):
            self._data      = json.dumps(data)
            self._loaded    = self._load()
            if isinstance(self._loaded, list):
                self._mode  = 'list'
            elif isinstance(self._loaded, dict):
                self._mode  = 'dict'
            elif isinstance(self._loaded, str) or isinstance(self._loaded, unicode) or isinstance(data, int):
                self._mode  = 'str'
            else:
                logging.warning('Unknown type: {}'.format(type(self._loaded)))

    def _load(self):
        return json.loads(self._data)

    def iterate_over_field(self, field):
        logging.debug('Iterating over {}'.format(field))
        return DBInfo([x for x in self._loaded[field]])

    def filter_by(self, filter_dict):
        if self._mode<>'list':
            raise DBActionError('You can filter only list of objects')
        return DBInfo([x for x in self._loaded if self._is_valid_field(x, filter_dict)])

    @staticmethod
    def _is_valid_field(d, filter_dict):
        for k in filter_dict.keys():
            if isinstance(filter_dict[k], list):
                if d[k] not in filter_dict[k]:
                    return False
            else:
                if d[k].upper() <> filter_dict[k].upper():
                    return False
        return True

    def loc(self, ind):
        #print('Getting by index {}'.format(ind))
        if self._mode == 'list':
            if len(self._loaded)>0:
                ret = self._loaded[ind]
            else:
                raise DBParseError('Cannot get by index from empty list.')
            #print(ret)
        else:
            raise DBParseError('Cannot apply loc for non-list object.')
        return DBInfo(ret)

    def get_field(self, field):
        #print('Getting field {}'.format(field))
        if self._mode == 'dict':
            return DBInfo(self._loaded[field])

        elif self._mode == 'list':
            ret = []
            for obj in self._loaded:
                if isinstance(obj, list):
                    try:
                        ret.append(obj[0][field])
                    except Exception:
                        ret.append('')
                else:
                    #if len(self._loaded)>0:
                    #    #print(sorted(self._loaded[0].keys()))
                    try:
                        ret.append(obj[field])
                    except Exception:
                        logging.warning('adding nothing')
                        ret.append('')
            return DBInfo(ret)

    def apply(self, action=None, function=None, **kwargs):
        logging.debug('Applying {}'.format(action if action else 'function'))
        if action=='download_json':
            URL_formatter = kwargs['URL_formatter']

            if self._mode == 'dict':
                raise Exception('?')

            elif self._mode == 'str':
                URL         = URL_formatter.format(self._loaded)
                response    = requests.get(URL, headers=HEADERS)
                file_json   = response.json()
                return DBInfo(file_json)

            elif self._mode == 'list':
                ret = []
                for obj in self._loaded:
                    URL         = URL_formatter.format(obj)
                    response    = requests.get(URL, headers=HEADERS)
                    file_json   = response.json()
                    ret.append(file_json)
                return DBInfo(ret)

        elif hasattr(function, '__call__'):
            if self._mode == 'str':
                return function(self._loaded)
            elif self._mode == 'list':
                return([function(x) for x in self._loaded])

    def __iter__(self):
        if self._mode=='list':
            return iter(self._loaded)
        else:
            return DBInfo(self._loaded)

    def __len__(self):
        if self._mode=='list':
            return len(self._loaded)
        else:
            return 1

    def __str__(self):
        return self._data

    def stringify(self, hard=False):
        if self._mode == 'dict':
            return self._data

        elif self._mode == 'list':
            ret = []
            for obj in self._loaded:
                ret.append(str(obj).strip())
            if not hard:
                return ret
            else:
                return ','.join([str(x).strip() for x in ret])

        elif self._mode == 'str':
            return str(self._loaded)


class FileProcessor():
    ### Processing: gff/bed/gz conversion, HOMER run and so on.
    def __init__(self, filename, genome_assembly='hg19', format_from=None):

        if not genome_assembly in ['ce10', 'dm3', 'mm10', 'hg19']:
            if 'ce' in genome_assembly:
                switch_to_version = 'ce10'
            elif 'dm' in genome_assembly:
                switch_to_version = 'dm3'
            elif 'mm' in genome_assembly:
                switch_to_version = 'mm10'
            elif 'hg' in genome_assembly:
                switch_to_version = 'hg19'
            else:
                raise DBProcessError('Genome is not supported. Filter out unsupported datasets, please.')
        else:
            switch_to_version = None
        self._switch = [genome_assembly, switch_to_version]
        self._genome = switch_to_version if switch_to_version else genome_assembly   # resulting version of genome
        self._file = filename.replace('(', '\(').replace(')', '\)').replace('=', '\=')

        if format_from == None:
            format_from = []
            for postfix in ['.gz', '.bed', '.gff3']:  ### TODO: add homer data format
                if postfix in self._file.lower():
                    format_from.append(postfix[1:])
            self._postfix_list = format_from

        elif isinstance(format_from, str):
            self._postfix_list = list([x for x in format_from.split('.')])

        elif isinstance(format_from, list):
            self._postfix_list = list([x if not x.startswith('.') else x[1:] for x in format_from])

        s = self._file.split('.')
        for p in self._postfix_list:
            s = [i for i in s if i.lower() <> p.lower()]
        self._file_basename = '.'.join(s)
        logging.debug("Filename info: {}\t{}\t{}".format(self._file_basename, self._file, self._postfix_list))

    def run_pipeline(self, mode='bash', clear_bash=False, HOMERpath=None, LIFTOVERpath='./liftover_lib/'):
        '''
        Run whole pipeline for homer files preparation and computations by python or bash commands.
        Note that in bash mode numerous files are generated, thus clear_bush option strongly recommended.

        :param mode: bash, python (not supported yet)
        :param LIFTOVERpath: str, should be present with switch_to_version for __init__
        :return:
        '''

        ### TODO add score!

        if not hasattr(self, '_postfix_list') or len(self._postfix_list)==0:
            raise DBProcessError('Postfix list is not defined. Filename: {}'.format(self._file))
        elif 'bed' in self._postfix_list and ('gff3' in self._postfix_list):
            raise DBProcessError('Contradictory postfixes: {}'.format(self._postfix_list))

        if not HOMERpath:
            self.findHOMERdir()
        else:
            self._HOMERpath = HOMERpath

        if self._switch[1] and not LIFTOVERpath:
            raise DBProcessError('LIFTOVER directory not specified!')
        elif self._switch[1] and not os.path.isdir(LIFTOVERpath):
            raise DBProcessError('LIFTOVER directory not specified!')
        elif self._switch[1]:
            self._switch_chainfile = os.path.abspath(LIFTOVERpath)+'/'+self._switch[0]+'To'+self._switch[1].title()+'.over.chain.gz'
            self._LIFTOVERpath = LIFTOVERpath
            if not os.path.exists(self._switch_chainfile):
                raise DBProcessError('LIFTOVER chain file for {} genome with conversion to {} (filename {}) not exists!'.format(self._switch[0], self._switch[1], self._switch_chainfile))

        if mode=='bash':
            if not self._switch[1]:
                logging.info("\t\tfile2homer is running...")
                self.file2homer()
            else:
                logging.info("\t\tfile2homer through liftover is running...")
                self.file2homer3lift()

            self._HOMERfile = "{}.homer".format(self._file_basename)
            logging.info("\t\tHOMER is running...")
            self.runHOMER()

            if clear_bash == True:
                is_err = call_and_check_errors("rm  {}.homer".format(self._file_basename))
                if is_err:
                    logging.error('Removal unsuccessful')
                del self._HOMERfile

        return 0

    def findHOMERdir(self):
        a = subprocess.check_output("which homer", stderr=subprocess.STDOUT, shell=True)

        try:
            command = '/'.join(a.split('/')[:-1])+'/annotatePeaks.pl'
            logging.debug("Looking for homer annotatePeaks.pl running... {}".format(command))
            subprocess.check_output(command, shell=True,  stderr=subprocess.STDOUT)
        except Exception as e:
            raise DBProcessError("HOMER not found in the system. Please, specify annotatePeaks.pl directory and add HOMER directory to path. ")
        self._HOMERpath = '/'.join(a.split('/')[:-1])


    def runHOMER(self, gene_annot=None, HOMERpath=None):

        if not HOMERpath and not self._HOMERpath:
            raise DBProcessError("No HOMER bin directory found nor specified")
        elif HOMERpath:
            self._HOMERpath = HOMERpath
        is_err = self.call_and_check_errors("perl {0}/annotatePeaks.pl {1} {2} > {3}".format(self._HOMERpath, self._HOMERfile, self._genome, self._file_basename+".homer_out"))
        if "!!!" in is_err:  #for HOMER !!! means the presence of error at some step
            logging.error("HOMER run failed for {}".format(self._HOMERfile))
        #"E.g.: perl bin/annotatePeaks.pl ex_sh.bed dm6 > ex_out.txt"

    def file2bed(self, force_non_gz=False):
        if force_non_gz:
            self._postfix_list = [i for i in self._postfix_list if i.lower()<>'gz']

        if 'gz' in self._postfix_list and 'bed' in self._postfix_list:
            command = str(r"""gzip -d -c {} | awk -F'\t' -v OFS='\t' '{{print $1, $2, $3, ".", $5 }}' > {}.bed """).format(self._file, self._file_basename)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.warning("file2bed failed. Retrying with no gz... Filename: {}".format(self._file))
                self.file2bed(force_non_gz=True)
        elif 'gz' in self._postfix_list and 'gff3' in self._postfix_list:
            command = str(r"""gzip -d -c {} | awk -F'\t': '/^[^#]/ {{ print $0 }}' | awk -F'\t' -v OFS='\t' '{{ gsub("\\.","1",$4); print "chr"$1, $4, $5, ".", $6 }}' > {}.bed """).format(self._file, self._file_basename)
            logging.info("%s", command)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.warning("file2bed failed. Retrying with no gz... Filename: {}".format(self._file))
                self.file2bed(force_non_gz=True)
        elif 'gff3' in self._postfix_list:
            command = str(
                r"""cat {} | awk -F'\t': '/^[^#]/ {{ print $0 }}' | awk -F'\t' -v OFS='\t' '{{ gsub("\\.","1",$4); print "chr"$1, $4, $5, ".", $6 }}' > {}.bed """).format(
                self._file, self._file_basename)
            logging.info("%s", command)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.error("file2bed failed eventually. Filename: {}".format(self._file))
        elif 'bed' in self._postfix_list:
            pass
        else:
            raise DBProcessError('Filetype not supported yet: {}'.format(self._postfix_list))


    def file2homer(self, force_non_gz=False):
        logging.info("Running file2homer for file: {}".format(self._file))

        if force_non_gz:
            self._postfix_list = [i for i in self._postfix_list if i.lower()<>'gz']

        if 'gz' in self._postfix_list and 'bed' in self._postfix_list:
            command = str(r"""gzip -d -c {} | awk -v OFS='\t' '{{gsub("\\.","1",$2); print NR-1, $1, $2, $3, "+", $5}}' > {}.homer """).format(self._file, self._file_basename)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.warning("file2homer failed. Rerunning with no gz. Filename: {}".format(self._file))
                self.file2homer(force_non_gz=True)
        elif 'gz' in self._postfix_list and 'gff3' in self._postfix_list:
            command = str(r"""gzip -d -c {} | awk -F'\t': '/^[^#]/ {{ print $0 }}' | awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$4); print NR-1, "chr"$1, $4, $5, "+", $6}}' > {}.homer """).format(self._file, self._file_basename)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.warning("file2homer failed. Rerunning with no gz. Filename: {}".format(self._file))
                self.file2homer(force_non_gz=True)
        elif 'bed' in self._postfix_list:
            command = str(
                r"""awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$2); print NR-1, $1, $2, $3, "+", $5}}' {} > {}.homer """).format(
                self._file, self._file_basename)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.error("file2homer failed. Filename: {}".format(self._file))
        elif 'gff3' in self._postfix_list:
            command = str(r"""cat {} | awk -F'\t': '/^[^#]/ {{ print $0 }}' | awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$4); print NR-1, "chr"$1, $4, $5, "+", $6}}' > {}.homer """).format(self._file, self._file_basename)
            is_err = self.call_and_check_errors(command)
            if is_err:
                logging.error("file2homer failed. Filename: {}".format(self._file))
        else:
            raise DBProcessError('Filetype not supported yet: {}'.format(self._postfix_list))

    def file2homer3lift(self, force_non_gz=False):
        if force_non_gz:
            self._postfix_list = [i for i in self._postfix_list if i.lower()<>'gz']

        if 'gz' in self._postfix_list and 'bed' in self._postfix_list:

            command1 = str(r"""gzip -d -c {} | awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$2); print $1, $2, $3, ".", $5}}' > {}.bed """).format(self._file, self._file_basename)
            command2 = str(r"{0}liftOver {1}.bed {2} {1}.bed_switched {1}.bed_non_switched").format(self._LIFTOVERpath, self._file_basename, self._switch_chainfile)
            command3 = str(r"""awk -F'\t' -v OFS='\t' '{{print NR-1, $1, $2, $3, "+", $5}}' {0}.bed_switched > {0}.homer """).format(self._file_basename)

            is_err1 = self.call_and_check_errors(command1)
            is_err2 = self.call_and_check_errors(command2)
            is_err3 = self.call_and_check_errors(command3)
            if is_err1 or (not 'Mapping coordinates' in is_err2) or is_err3:
                logging.error("file2homer3lift failed. Rerunning with no gz. Filename:{}".format(self._file))
                self.file2homer3lift(force_non_gz=True)

        elif 'gz' in self._postfix_list and 'gff3' in self._postfix_list:

            command1 = str(r"""gzip -d -c {} | awk -F'\t': '/^[^#]/ {{ print $0 }}' | awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$4); print "chr"$1, $4, $5, ".", $6}}' > {}.bed """).format(self._file, self._file_basename)
            command2 = str(r"{0}liftOver {1}.bed {2} {1}.bed_switched {1}.bed_non_switched").format(self._LIFTOVERpath, self._file_basename, self._switch_chainfile)
            command3 = str(r"""awk -F'\t' -v OFS='\t' '{{print NR-1, $1, $2, $3, "+", $5}}' {0}.bed_switched > {0}.homer """).format(self._file_basename)

            is_err1 = self.call_and_check_errors(command1)
            is_err2 = self.call_and_check_errors(command2)
            is_err3 = self.call_and_check_errors(command3)
            if is_err1 or (not 'Mapping coordinates' in is_err2) or is_err3:
                logging.error("file2homer3lift failed. Rerunning with no gz. Filename:{}".format(self._file))
                self.file2homer3lift(force_non_gz=True)

        elif 'bed' in self._postfix_list:

            command1 = str(r"""awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$2); print $1, $2, $3, ".", $5}}' {} > {}.bed1 """).format(self._file, self._file_basename)
            command2 = str(r"{0}liftOver {1}.bed1 {2} {1}.bed_switched {1}.bed_non_switched").format(self._LIFTOVERpath, self._file_basename, self._switch_chainfile)
            command3 = str(r"""awk -F'\t' -v OFS='\t' '{{print NR-1, $1, $2, $3, "+", $5}}' {0}.bed_switched > {0}.homer """).format(self._file_basename)

            is_err1 = self.call_and_check_errors(command1)
            is_err2 = self.call_and_check_errors(command2)
            is_err3 = self.call_and_check_errors(command3)
            if is_err1 or (not 'Mapping coordinates' in is_err2) or is_err3:
                logging.error("file2homer3lift failed for {}".format(self._file))

        elif 'gff3' in self._postfix_list:

            command1 = str(r"""cat {} | awk -F'\t': '/^[^#]/ {{ print $0 }}' | awk -F'\t' -v OFS='\t' '{{gsub("\\.","1",$4); print "chr"$1, $4, $5, ".", $6}}' > {}.bed """).format(self._file, self._file_basename)
            command2 = str(r"{0}liftOver {1}.bed {2} {1}.bed_switched {1}.bed_non_switched").format(self._LIFTOVERpath, self._file_basename, self._switch_chainfile)
            command3 = str(r"""awk -F'\t' -v OFS='\t' '{{print NR-1, $1, $2, $3, "+", $5}}' {0}.bed_switched > {0}.homer """).format(self._file_basename)

            is_err1 = self.call_and_check_errors(command1)
            is_err2 = self.call_and_check_errors(command2)
            is_err3 = self.call_and_check_errors(command3)
            if is_err1 or (not 'Mapping coordinates' in is_err2) or is_err3:
                logging.error("file2homer3lift failed for {}".format(self._file))

        else:
            raise DBProcessError('Filetype not supported yet: {}'.format(self._postfix_list))

    @staticmethod
    def call_and_check_errors(command):
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdout, stderr) = proc.communicate()
        if stderr:
            logging.info("Stderr is not empty. Might be an error in call_and_check_errors for the command: {}".format(command))
            logging.info("Check stderr: {}".format(stderr))
            return stderr   # Error, very bad!
        else:
            return 0        # No error, great!
